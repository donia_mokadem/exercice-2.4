package fr.cnam.foad.nfa035.badges.wallet.model;

import java.io.File;
import java.util.Date;
import java.util.Objects;

/**
 * POJO model représentant le Badge Digital
 */
public class DigitalBadge {

    private DigitalBadgeMetadata metadata;
    private File badge;
    private Date begin;
    private Date end;
    private String serial;

    /**
     * Constructeur
     *
     * @param metadata
     * @param badge
     */
    public DigitalBadge(DigitalBadgeMetadata metadata, File badge) {
        this.metadata = metadata;
        this.badge = badge;
    }

    /**
     * Getter des métadonnées du badge
     * @return les métadonnées DigitalBadgeMetadata
     */
    public DigitalBadgeMetadata getMetadata() {
        return metadata;
    }
   
    
    /**Getter begin (d�but)
     * 
     * @return begin
     */
    public Date getBegin() {
        return begin;
    }
    /** Setter begin (d�but)
     * 
     * @param begin
     */
    public void setBegin(Date begin) {
        this.begin = begin;
    }
    /**Getter end (fin)
     * 
     * @return fin
     */
    public Date getEnd() {
        return end;
    }
    /** Setter end (fin)
     * 
     * @param end
     */
    public void setEnd(Date end) {
        this.end = end;
    }
    /**Getter serial
     * 
     * @return serial
     */
    public String getSerial(String serial) {
        return serial;
    }
    /** Setter serial
     * 
     * @param serial
     */
    public void setSerial(String serial) {
        this.serial = serial;
    }
    /**
     * Setter des métadonnées du badge
     * @param metadata
     */
    public void setMetadata(DigitalBadgeMetadata metadata) {
        this.metadata = metadata;
    }

    /**
     * Getter du badge (l'image)
     * @return le badge (File)
     */
    public File getBadge() {
        return badge;
    }

    /**
     * Setter du badge (Fichier image)
     * @param badge
     */
    public void setBadge(File badge) {
        this.badge = badge;
    }

    /**
     * {@inheritDoc}
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DigitalBadge that = (DigitalBadge) o;
        return metadata.equals(that.metadata) && badge.equals(that.badge);
    }

    /**
     * {@inheritDoc}
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(metadata, badge);
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public String toString() {
        return "DigitalBadge{" +
                "metadata=" + metadata +
                ", badge=" + badge +
                '}';
    }
}
